function validarNumeroDni() {
    
    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

    var numeroDni = prompt("Introduce tu número de DNI (sin la letra)");

    var letraDni = prompt("Introduce la letra de tu DNI (en mayúsculas)").toUpperCase();

    var calculoLetra = letras[numeroDni % 23];

    if ((numeroDni < 0) && (numeroDni > 99999999)) {
        console.log("Número de DNI proporcionado no válido");
    } else if (calculoLetra != letraDni) {
            alert("La letra del DNI introducida es incorrecta");
    } else {
            alert("DNI VÁLIDO");
    }
}

validarNumeroDni();
